# EcommerceAdminDashboard
This project is an Ecommerce Administrative Dashboard. It was built from scratch using HTML, CSS and JAVASCRIPT.
To see a live demo of this project, please click [here](https://immanuel5015.github.io/EcommerceAdminDashboard/ecommerce.html)
